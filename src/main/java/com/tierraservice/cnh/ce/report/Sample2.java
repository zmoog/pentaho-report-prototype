package com.tierraservice.cnh.ce.report;

/*
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License, version 2.1 as published by the Free Software
 * Foundation.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, you can obtain a copy at http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * or from the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * Copyright 2009 Pentaho Corporation.  All rights reserved.
 *
 * Created July 22, 2009
 * @author dkincade
 */

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.pentaho.reporting.engine.classic.core.DataFactory;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.pentaho.reporting.engine.classic.core.ReportProcessingException;
import org.pentaho.reporting.engine.classic.core.wizard.RelationalAutoGeneratorPreProcessor;
import org.pentaho.reporting.engine.classic.core.modules.misc.datafactory.sql.SQLReportDataFactory;
import org.pentaho.reporting.engine.classic.core.modules.misc.datafactory.sql.DriverConnectionProvider;

/**
 * Generates a report in the following scenario:
 * &lt;ol>
 * &lt;li>The report definition file is a .prpt file which will be loaded and parsed
 * &lt;li>The data factory is a simple JDBC data factory using HSQLDB
 * &lt;li>There are no runtime report parameters used
 * &lt;/ol>
 */
public class Sample2 extends AbstractReportGenerator
{
    private static final String QUERY_NAME = "ReportQuery";

    /**
     * Default constructor for this sample report generator
     */
    public Sample2()
    {
    }

    /**
     * Returns the report definition which will be used to generate the report. In this case, the report will be
     * loaded and parsed from a file contained in this package.
     *
     * @return the loaded and parsed report definition to be used in report generation.
     */
    public MasterReport getReportDefinition()
    {
        final MasterReport report = new MasterReport();
        report.setQuery(QUERY_NAME);
        report.addPreProcessor(new RelationalAutoGeneratorPreProcessor());
        return report;
    }

    /**
     * Returns the data factory which will be used to generate the data used during report generation. In this example,
     * we will return null since the data factory has been defined in the report definition.
     *
     * @return the data factory used with the report generator
     */
    public DataFactory getDataFactory()
    {
        final DriverConnectionProvider sampleDriverConnectionProvider = new DriverConnectionProvider();
        sampleDriverConnectionProvider.setDriver("org.hsqldb.jdbcDriver");
        // sampleDriverConnectionProvider.setUrl("jdbc:hsqldb:./sql/sampledata");
        sampleDriverConnectionProvider.setUrl("jdbc:hsqldb:res:/sql/sampledata");
        sampleDriverConnectionProvider.setProperty("user", "sa");
        sampleDriverConnectionProvider.setProperty("password", "");

        final SQLReportDataFactory dataFactory = new SQLReportDataFactory(sampleDriverConnectionProvider);
        dataFactory.setQuery(QUERY_NAME,
                "select CUSTOMERNAME, CITY, STATE, POSTALCODE, COUNTRY from CUSTOMERS order by UPPER(CUSTOMERNAME)");

        return dataFactory;
    }

    /**
     * Returns the set of runtime report parameters. This sample report does not use report parameters, so the
     * method will return &lt;code>null&lt;/code>
     *
     * @return &lt;code>null&lt;/code> indicating the report generator does not use any report parameters
     */
    public Map<String, Object> getReportParameters()
    {
        return null;
    }

    public static void main(String[] args) throws IOException, ReportProcessingException
    {
        // Create an output filenames
        final File xlsOutputFilename = new File(Sample2.class.getSimpleName() + ".xls");
        final File pdfOutputFilename = new File(Sample2.class.getSimpleName() + ".pdf");
        final File htmlOutputFilename = new File(Sample2.class.getSimpleName() + ".html");

        // Generate the reports
        new Sample2().generateReport(OutputType.EXCEL, xlsOutputFilename);
        new Sample2().generateReport(OutputType.PDF, pdfOutputFilename);
        new Sample2().generateReport(OutputType.HTML, htmlOutputFilename);

        // Output the location of the files
        System.err.println("Generated the report [" + xlsOutputFilename.getAbsolutePath() + "]");
        System.err.println("Generated the report [" + pdfOutputFilename.getAbsolutePath() + "]");
        System.err.println("Generated the report [" + htmlOutputFilename.getAbsolutePath() + "]");
    }
}
