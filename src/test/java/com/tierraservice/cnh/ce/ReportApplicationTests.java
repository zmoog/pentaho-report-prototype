package com.tierraservice.cnh.ce;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.pentaho.reporting.engine.classic.core.MasterReport;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReportApplication.class)
@WebAppConfiguration
public class ReportApplicationTests {

	@Test
	public void contextLoads() {

	}

}
